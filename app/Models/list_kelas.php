<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class list_kelas extends Model
{
    use HasFactory;
    protected $table = 'list_kelas';
    protected $fillable = [
        'id_list_kelas',
        'id_kelas',
        'id_student',
        'id_teacher',
        'id_semester',
        'id_tahun_ajaran',
        'status',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];

    function kelas()
    {
        return $this->hasOne(kelas::class, 'id_kelas', 'id_kelas');
    }

    function teacher()
    {
        return $this->hasOne(users::class, 'id_user', 'id_teacher');
    }

    function student()
    {
        return $this->hasOne(users::class, 'id_user', 'id_student');
    }

    function semester()
    {
        return $this->hasOne(semester::class, 'id_semester', 'id_semester');
    }

    function tahun_ajaran()
    {
        return $this->hasOne(users::class, 'id_tahun_ajaran', 'id_tahun_ajaran');
    }
}
