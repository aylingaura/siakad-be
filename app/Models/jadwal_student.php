<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class jadwal_student extends Model
{
    use HasFactory;
    protected $table = 'jadwal_student';
    protected $fillable = [
        'id_jadwal_student',
        'id_jadwal',
        'id_student',
        'is_open',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];

    function jadwal()
    {
        return $this->hasOne(jadwal::class, 'id_jadwal', 'id_jadwal');
    }

    function student()
    {
        return $this->hasOne(users::class, 'id_user', 'id_student');
    }
}
