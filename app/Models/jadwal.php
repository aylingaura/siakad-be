<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class jadwal extends Model
{
    use HasFactory;
    protected $table = 'm_jadwal';
    protected $fillable = [
        'id_jadwal',
        'id_teacher',
        'id_kelas',
        'id_semester',
        'id_tahun_ajaran',
        'id_mata_pelajaran',
        'id_ruangan',
        'date',
        'start_time',
        'end_time',
        'is_open',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];

    function kelas()
    {
        return $this->hasOne(kelas::class, 'id_kelas', 'id_kelas');
    }

    function mata_pelajaran()
    {
        return $this->hasOne(mata_pelajaran::class, 'id_mata_pelajaran', 'id_mata_pelajaran');
    }

    function semester()
    {
        return $this->hasOne(semester::class, 'id_semester', 'id_semester');
    }

    function tahun_ajaran()
    {
        return $this->hasOne(tahun_ajaran::class, 'id_tahun_ajaran', 'id_tahun_ajaran');
    }

    function ruangan()
    {
        return $this->hasOne(ruangan::class, 'id_ruangan', 'id_ruangan');
    }
}
