<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class users extends Model
{
    use HasFactory;
    protected $table = 'm_user';
    protected $fillable = [
        'name',
        'id_user_type',
        'pob',
        'dob',
        'id_province',
        'id_city',
        'id_district',
        'id_village',
        'education',
        'email',
        'phone',
        'photo',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];

    function user_type()
    {
        return $this->hasOne(user_type::class, 'id_user_type', 'id_user_type');
    }
}
