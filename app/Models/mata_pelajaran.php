<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class mata_pelajaran extends Model
{
    use HasFactory;
    protected $table = 'm_mata_pelajaran';
    protected $fillable = [
        'id_mata_pelajaran', 
        'name', 
        'description', 
        'status', 
        'created_at', 
        'created_by', 
        'updated_at', 
        'updated_by'
    ];

}
