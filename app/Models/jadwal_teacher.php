<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class jadwal_teacher extends Model
{
    use HasFactory;
    protected $table = 'jadwal_teacher';
    protected $fillable = [
        'id_jadwal_teacher',
        'id_jadwal',
        'id_teacher',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];

    function jadwal()
    {
        return $this->hasOne(jadwal::class, 'id_jadwal', 'id_jadwal');
    }

    function teacher()
    {
        return $this->hasOne(users::class, 'id_user', 'id_teacher');
    }
}
