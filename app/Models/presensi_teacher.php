<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class presensi_teacher extends Model
{
    use HasFactory;
    protected $table = 'presensi_teacher';
    protected $fillable = [
        'id_presensi_teacher',
        'id_jadwal_teacher',
        'id_jadwal',
        'is_present',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];

    function jadwal_teacher()
    {
        return $this->hasOne(jadwal_teacher::class, 'id_jadwal_teacher', 'id_jadwal_teacher');
    }

    function jadwal()
    {
        return $this->hasOne(jadwal::class, 'id_jadwal', 'id_jadwal');
    }
}
