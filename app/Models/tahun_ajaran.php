<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tahun_ajaran extends Model
{
    use HasFactory;
    protected $table = 'm_tahun_ajaran';
    protected $fillable = [
        'id_tahun_ajaran', 
        'name', 
        'description', 
        'status', 
        'created_at', 
        'created_by', 
        'updated_at', 
        'updated_by'
    ];

}
