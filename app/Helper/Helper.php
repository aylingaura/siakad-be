<?php
namespace App\Helper;

class Helper
{
    static function queryParam($param, $table)
    {
        return $table::whereNested(function($query) use ($param)
        {
            foreach ($param as $key => $value)
            {
                if($value != ''){
                    $query->where($key, 'like', '%' . $value . '%');
                }
            }
        }, 'and');
    }
}
