<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;
use App\Helper\Helper;

use App\Models\ruangan;

class RuanganController extends Controller
{
    protected $data_req = [
        'name' => 'required'
    ];

    public function index(Request $request)
    {
        try {
            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => Helper::queryParam($request->all(), 'App\Models\ruangan')->orderBy('updated_at', 'DESC')->get()
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function detail($id_ruangan)
    {
        try {

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => ruangan::where('id_ruangan', $id_ruangan)->first()
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function create(Request $request)
    {
        try {

            $input = $request->except(['created_at', 'created_by', 'updated_at', 'updated_by']);

            /** Validate input */
            $validator = Validator::make($request->all(), $this->data_req);

            /** chack validate input */
            if ($validator->fails()) {
                return response()->json([
                    'status'    => 501,
                    'errorText' => 'Validation failed',
                    'message'   => $validator->errors()
                ], 501);
            }

            ruangan::create(array_merge($input, [
                'created_by' => $request->user()->username,
                'id_ruangan' => ruangan::orderBy('id_ruangan', 'desc')->first()->id_ruangan + 1
            ]));

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => $request->all()
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function update($id_ruangan, Request $request)
    {
        try {

            $input = $request->except(['created_at', 'created_by', 'updated_at', 'updated_by']);

            /** Validate input */
            $validator = Validator::make($request->all(), $this->data_req);

            /** chack validate input */
            if ($validator->fails()) {
                return response()->json([
                    'status'    => 501,
                    'errorText' => 'Validation failed',
                    'message'   => $validator->errors()
                ], 501);
            }

            ruangan::where('id_ruangan', $id_ruangan)->update(array_merge($input, ['updated_by' => $request->user()->username]));

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => $request->all()
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function delete($id_ruangan)
    {
        try {
            $data = ruangan::where('id_ruangan', $id_ruangan)->first();
            ruangan::where('id_ruangan', $id_ruangan)->delete();

            return response()->json([
                'status'  => $data ? 200 : 500,
                'message' => $data ? 'delete success' : 'delete failed',
                'data'    => $data
            ], $data ? 200 : 500);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }
}
