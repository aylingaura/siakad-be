<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;
use App\Helper\Helper;

use App\Models\mata_pelajaran;

class MataPelajaranController extends Controller
{
    protected $data_req = [
        'name' => 'required'
    ];

    public function index(Request $request)
    {
        try {
            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => Helper::queryParam($request->all(), 'App\Models\mata_pelajaran')->orderBy('updated_at', 'DESC')->get()
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function detail($id_mata_pelajaran)
    {
        try {

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => mata_pelajaran::where('id_mata_pelajaran', $id_mata_pelajaran)->first()
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function create(Request $request)
    {
        try {

            $input = $request->except(['created_at', 'created_by', 'updated_at', 'updated_by']);

            /** Validate input */
            $validator = Validator::make($request->all(), $this->data_req);

            /** chack validate input */
            if ($validator->fails()) {
                return response()->json([
                    'status'    => 501,
                    'errorText' => 'Validation failed',
                    'message'   => $validator->errors()
                ], 501);
            }

            mata_pelajaran::create(array_merge($input, [
                'created_by' => $request->user()->username,
                'id_mata_pelajaran' => mata_pelajaran::orderBy('id_mata_pelajaran', 'desc')->first()->id_mata_pelajaran + 1
            ]));

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => $request->all()
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function update($id_mata_pelajaran, Request $request)
    {
        try {

            $input = $request->except(['created_at', 'created_by', 'updated_at', 'updated_by']);

            /** Validate input */
            $validator = Validator::make($request->all(), $this->data_req);

            /** chack validate input */
            if ($validator->fails()) {
                return response()->json([
                    'status'    => 501,
                    'errorText' => 'Validation failed',
                    'message'   => $validator->errors()
                ], 501);
            }

            mata_pelajaran::where('id_mata_pelajaran', $id_mata_pelajaran)->update(array_merge($input, ['updated_by' => $request->user()->username]));

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => $request->all()
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function delete($id_mata_pelajaran)
    {
        try {
            $data = mata_pelajaran::where('id_mata_pelajaran', $id_mata_pelajaran)->first();
            mata_pelajaran::where('id_mata_pelajaran', $id_mata_pelajaran)->delete();

            return response()->json([
                'status'  => $data ? 200 : 500,
                'message' => $data ? 'delete success' : 'delete failed',
                'data'    => $data
            ], $data ? 200 : 500);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }
}
