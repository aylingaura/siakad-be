<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;
use App\Helper\Helper;

use App\Models\user_type;

class UserTypeController extends Controller
{
    protected $data_req = [
        'name' => 'required'
    ];

    public function index(Request $request)
    {
        try {
            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => Helper::queryParam($request->all(), 'App\Models\user_type')->orderBy('updated_at', 'DESC')->get()
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function detail($id_user_type)
    {
        try {

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => user_type::where('id_user_type', $id_user_type)->first()
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function create(Request $request)
    {
        try {

            $input = $request->except(['created_at', 'created_by', 'updated_at', 'updated_by']);

            /** Validate input */
            $validator = Validator::make($request->all(), $this->data_req);

            /** chack validate input */
            if ($validator->fails()) {
                return response()->json([
                    'status'    => 501,
                    'errorText' => 'Validation failed',
                    'message'   => $validator->errors()
                ], 501);
            }

            user_type::create(array_merge($input, [
                'created_by' => $request->user()->username,
                'id_user_type' => user_type::orderBy('id_user_type', 'desc')->first()->id_user_type + 1
            ]));

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => $request->all()
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function update($id_user_type, Request $request)
    {
        try {

            $input = $request->except(['created_at', 'created_by', 'updated_at', 'updated_by']);

            /** Validate input */
            $validator = Validator::make($request->all(), $this->data_req);

            /** chack validate input */
            if ($validator->fails()) {
                return response()->json([
                    'status'    => 501,
                    'errorText' => 'Validation failed',
                    'message'   => $validator->errors()
                ], 501);
            }

            user_type::where('id_user_type', $id_user_type)->update(array_merge($input, ['updated_by' => $request->user()->username]));

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => $request->all()
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function delete($id_user_type)
    {
        try {
            $data = user_type::where('id_user_type', $id_user_type)->first();
            user_type::where('id_user_type', $id_user_type)->delete();

            return response()->json([
                'status'  => $data ? 200 : 500,
                'message' => $data ? 'delete success' : 'delete failed',
                'data'    => $data
            ], $data ? 200 : 500);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }
}
