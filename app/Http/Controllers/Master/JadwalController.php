<?php

namespace App\Http\Controllers\Master;

use App\Models\list_kelas;
use App\Models\jadwal;
use App\Models\jadwal_student;
use App\Models\jadwal_teacher;
use App\Models\presensi_student;
use App\Models\presensi_teacher;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;
use App\Helper\Helper;

class JadwalController extends Controller
{
    public function create(Request $request)
    {
        try {

            $jadwal_student = [];
            $jadwal_teacher = '';
            $data = $request->except(['created_at', 'created_by', 'updated_at', 'updated_by']);

            $check_jadwal = jadwal::where('id_kelas', $request->id_kelas)
                ->where('id_teacher', $request->id_teacher)
                ->where('id_tahun_ajaran', $request->id_tahun_ajaran)
                ->where('id_mata_pelajaran', $request->id_mata_pelajaran)
                ->where('id_ruangan', $request->id_ruangan)
                ->where('date', $request->date)
                ->where('start_time', $request->start_time)
                ->where('end_time', $request->end_time)
                ->count();

            ## check jika jadwal sudah ada
            if ($check_jadwal < 1) {

                ## @create jadwal
                $jadwal = jadwal::create(array_merge($data, ['created_by' => $request->user()->username]));

                ## check jika jadwal berhasil di input
                if ($jadwal->id) {

                    ## list student
                    $students = list_kelas::where('id_kelas', $request->id_kelas)
                        ->where('id_semester', $request->id_semester)
                        ->where('id_tahun_ajaran', $request->id_tahun_ajaran)
                        ->select('id_student')
                        ->get();

                    ## @create jadwal_student
                    ## input jadwal student sesuai dengan id jadwal yang sudah ter input
                    foreach ($students as $key => $value) {

                        $jadwal_student[] = jadwal_student::create(array_merge(
                            [
                                'id_student' => $value->id_student,
                                'id_jadwal' => $jadwal->id,
                                'is_open' => '0'
                            ],
                            [
                                'created_by' => $request->user()->username
                            ]
                        ));
                    }

                    ## @create jadwal_teacher
                    $jadwal_teacher = jadwal_teacher::create(array_merge(
                        [
                            'id_teacher' => $request->id_teacher,
                            'id_jadwal' => $jadwal->id
                        ],
                        [
                            'created_by' => $request->user()->username
                        ]
                    ));

                    return response()->json([
                        'status'  => 200,
                        'message' => 'success',
                        'data'    => [
                            'jadwal' => $jadwal,
                            'jadwal_student' => $jadwal_student,
                            'jadwal_teacher' => $jadwal_teacher
                        ]
                    ], 200);
                } else {

                    return response()->json([
                        'status'  => 500,
                        'message' => 'Jadwal gagal tersimpan'
                    ], 500);
                }
            } else {

                return response()->json([
                    'status'  => 500,
                    'message' => 'Jadwal sudah terdaftar'
                ], 500);
            }
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function update($id_jadwal, Request $request)
    {
        try {

            $jadwal_student = [];
            $data = $request->except(['created_at', 'created_by', 'updated_at', 'updated_by']);

            $check_jadwal = jadwal::where('id_kelas', $request->id_kelas)
                ->where('id_teacher', $request->id_teacher)
                ->where('id_semester', $request->id_semester)
                ->where('id_tahun_ajaran', $request->id_tahun_ajaran)
                ->where('id_mata_pelajaran', $request->id_mata_pelajaran)
                ->where('id_ruangan', $request->id_ruangan)
                ->where('date', $request->date)
                ->where('start_time', $request->start_time)
                ->where('end_time', $request->end_time)
                ->count();

            ## check jika jadwal sudah ada
            if ($check_jadwal < 1) {

                ## @update jadwal
                $update_jadwal = jadwal::where('id_jadwal', $id_jadwal)->update(array_merge($data, ['updated_by' => $request->user()->username]));

                ## ============================ START UBAH JADWAL TEACHER ============================
                ## @update jadwal teacher
                $jadwal_teacher = jadwal_teacher::where('id_jadwal', $id_jadwal)->update(
                    array_merge(
                        ['id_teacher' => $request->id_teacher],
                        ['updated_by' => $request->user()->username]
                    )
                );
                ## ============================ END UBAH JADWAL TEACHER ============================

                ## ============================ START UBAH JADWAL STUDENT ============================
                ## @delete jadwal student
                $delete_jadwal_student = jadwal_student::where('id_jadwal', $id_jadwal)->delete();

                ## list student
                $students = list_kelas::where('id_kelas', $request->id_kelas)
                    ->where('id_semester', $request->id_semester)
                    ->where('id_tahun_ajaran', $request->id_tahun_ajaran)
                    ->select('id_student')
                    ->get();

                ## @create jadwal_student
                ## input jadwal student sesuai dengan id jadwal yang sudah ter input
                foreach ($students as $key => $value) {

                    $jadwal_student[] = jadwal_student::create(array_merge(
                        [
                            'id_student' => $value->id_student,
                            'id_jadwal' => $id_jadwal,
                            'is_open' => $request->is_open
                        ],
                        [
                            'created_by' => $request->user()->username
                        ]
                    ));
                }
                ## ============================ END UBAH JADWAL STUDENT ============================

                return response()->json([
                    'status'  => 200,
                    'message' => 'success',
                    'data'    => [
                        'jadwal' => $update_jadwal,
                        'jadwal_teacher' => $jadwal_teacher,
                        'delete_jadwal_student' => $delete_jadwal_student,
                        'jadwal_student' => $jadwal_student
                    ]
                ], 200);
            } else {

                return response()->json([
                    'status'  => 500,
                    'message' => 'Jadwal sudah terdaftar tidak bisa diubah'
                ], 500);
            }
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function delete($id_jadwal)
    {
        try {
            $jadwal = jadwal::where('id_jadwal', $id_jadwal)->delete();
            $student = jadwal_student::where('id_jadwal', $id_jadwal)->delete();
            $teacher = jadwal_teacher::where('id_jadwal', $id_jadwal)->delete();

            return response()->json([
                'status'  => 200,
                'message' => 'Berhasil hapus data',
                'data'    => [
                    'jadwal' => $jadwal,
                    'student' => $student,
                    'teacher' => $teacher
                ]
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }
}
