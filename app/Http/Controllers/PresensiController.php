<?php

namespace App\Http\Controllers;

use App\Helper\Helper;
use App\Models\list_kelas;
use App\Models\presensi;
use App\Models\jadwal;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PresensiController extends Controller
{
    public function student(Request $request)
    {
        try {

            $data = list_kelas::leftJoin('presensi', function ($join) {
                $join->on('list_kelas.id_list_kelas', '=', 'presensi.id_list_kelas');
            })
                ->leftJoin('jadwal', function ($join) {
                    $join->on('list_kelas.id_kelas', '=', 'jadwal.id_kelas');
                })
                ->select(
                    'list_kelas.id_kelas',
                    'list_kelas.id_teacher',
                    'id_student',
                    'is_present',
                    'id_jadwal',
                    'id_mata_pelajaran',
                    'jadwal.date',
                    'jadwal.start_time',
                    'jadwal.end_time'
                )
                ->with('kelas:id_kelas,name')
                ->with('student:id_user,name')
                ->with('teacher:id_user,name')
                ->where('list_kelas.id_kelas', '=', $request->id_kelas)
                ->where('list_kelas.id_student', '=', $request->id_student)
                ->where('list_kelas.id_teacher', '=', $request->id_teacher)
                ->where('jadwal.id_jadwal', '=', $request->id_jadwal)
                ->get();

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => $data
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function kelas(Request $request)
    {
        try {

            $data = list_kelas::leftJoin('presensi', function ($join) {
                $join->on('list_kelas.id_list_kelas', '=', 'presensi.id_list_kelas');
            })
                ->leftJoin('jadwal', function ($join) {
                    $join->on('list_kelas.id_kelas', '=', 'jadwal.id_kelas');
                })
                ->select(
                    'list_kelas.id_kelas',
                    'list_kelas.id_teacher',
                    'id_student',
                    'is_present',
                    'id_jadwal',
                    'id_mata_pelajaran',
                    'jadwal.date',
                    'jadwal.start_time',
                    'jadwal.end_time'
                )
                ->with('kelas:id_kelas,name')
                ->with('student:id_user,name')
                ->with('teacher:id_user,name')
                ->where('list_kelas.id_kelas', '=', $request->id_kelas)
                ->where('jadwal.id_jadwal', '=', $request->id_jadwal)
                ->get();

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => $data
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function teacher(Request $request)
    {
        try {

            $data = jadwal::leftJoin('presensi', function ($join) {
                $join->on('jadwal.id_jadwal', '=', 'presensi.id_jadwal');
            })
                ->select(
                    'jadwal.id_teacher',
                    'jadwal.id_mata_pelajaran',
                    'jadwal.id_kelas',
                    'jadwal.id_ruangan',
                    'jadwal.date',
                    'jadwal.start_time',
                    'jadwal.end_time',
                    'presensi.is_present'
                )
                ->with('teacher:id_user,name')
                ->with('kelas:id_kelas,name')
                ->with('mata_pelajaran:id_mata_pelajaran,name')
                ->with('ruangan:id_ruangan,name')
                ->where('jadwal.id_teacher', '=', $request->id_teacher)
                ->where('jadwal.id_kelas', '=', $request->id_kelas)
                ->where('jadwal.id_mata_pelajaran', '=', $request->id_mata_pelajaran)
                ->where('jadwal.id_ruangan', '=', $request->id_ruangan)
                ->where('jadwal.date', '=', $request->date)
                ->get();

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => $data
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }
}
