<?php

namespace App\Http\Controllers;

use App\Helper\Helper;

use App\Models\jadwal;
use App\Models\jadwal_student;
use App\Models\jadwal_teacher;
use App\Models\users;

use Illuminate\Http\Request;

class JadwalTeacherController extends Controller
{
    public function list($id_teacher, $date)
    {
        try {

            $teacher = users::where('id_user', $id_teacher)->get();

            $jadwal_teacher = jadwal::join('jadwal_teacher', function ($join) {
                $join->on('m_jadwal.id_jadwal', '=', 'jadwal_teacher.id_jadwal');
            })
                ->select(
                    'm_jadwal.id_jadwal',
                    'm_jadwal.id_kelas',
                    'm_jadwal.id_semester',
                    'm_jadwal.id_tahun_ajaran',
                    'm_jadwal.id_mata_pelajaran',
                    'm_jadwal.id_ruangan',
                    'm_jadwal.date',
                    'm_jadwal.start_time',
                    'm_jadwal.end_time',
                )
                ->where('jadwal_teacher.id_teacher', $id_teacher)
                ->where('m_jadwal.date', $date)
                ->with('kelas:id_kelas,name')
                ->with('semester:id_semester,name')
                ->with('tahun_ajaran:id_tahun_ajaran,name')
                ->with('mata_pelajaran:id_mata_pelajaran,name')
                ->with('ruangan:id_ruangan,name')
                ->get();

            $data = [
                'teacher' => $teacher,
                'jadwal' => $jadwal_teacher
            ];

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => $data
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function detail($id_teacher, $id_jadwal)
    {
        try {

            $teacher = jadwal_teacher::leftJoin('presensi_teacher', function ($join) {
                $join->on('jadwal_teacher.id_jadwal_teacher', '=', 'presensi_teacher.id_jadwal_teacher');
            })
                ->select(
                    'jadwal_teacher.id_jadwal_teacher',
                    'jadwal_teacher.id_jadwal',
                    'jadwal_teacher.id_teacher',
                    'presensi_teacher.is_present',
                )
                ->where('jadwal_teacher.id_jadwal', $id_jadwal)
                ->where('jadwal_teacher.id_teacher', $id_teacher)
                ->with('teacher:id_user,name,email')
                ->get();

            $jadwal = jadwal::where('id_jadwal', $id_jadwal)
                ->with('kelas:id_kelas,name')
                ->with('semester:id_semester,name')
                ->with('tahun_ajaran:id_tahun_ajaran,name')
                ->with('mata_pelajaran:id_mata_pelajaran,name')
                ->with('ruangan:id_ruangan,name')
                ->get();

            $student = jadwal_student::leftJoin('presensi_student', function ($join) {
                $join->on('jadwal_student.id_jadwal_student', '=', 'presensi_student.id_jadwal_student');
            })
                ->select(
                    'jadwal_student.id_jadwal_student',
                    'jadwal_student.id_jadwal',
                    'jadwal_student.id_student',
                    'presensi_student.is_present',
                )
                ->where('jadwal_student.id_jadwal', $id_jadwal)
                ->with('student:id_user,name,email')
                ->get();

            $data = [
                'teacher' => $teacher,
                'jadwal' => $jadwal,
                'student' => $student
            ];

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => $data
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }
}
