<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Arr;

use App\Models\User;
use App\Models\users;
use App\Models\OauthAccessTokens;

class AuthController extends Controller
{
    /**
     * Register
     */
    public function register(Request $request)
    {
        try {

            $input = $request->except(['created_at', 'created_by', 'updated_at', 'updated_by']);

            /** Validate input */
            $validator = Validator::make($request->all(), [
                'email'             => 'required|email',
                'phone'             => 'required',
                'name'              => 'required',
                'username'          => 'required',
                'id_user'           => 'required',
                'password'          => 'required',
                'confirm_password'  => 'required|same:password',
            ]);

            /** chack validate input */
            if ($validator->fails()) {
                return response()->json([
                    'status'  => 501,
                    'message' => $validator->errors()
                ], 501);
            }

            /** check exist username */
            $existUser = User::where('username', '=', $request->username)->first();

            if ($existUser) {
                return response()->json([
                    'status'  => 200,
                    'message' => 'username already registered'
                ], 200);
            }

            /** 
             * Create master user first then create user login 
             */
            try {

                /** store data to database */
                $user  = users::create(array_merge($input, ['created_by' => 'new user']));
                $input = array_merge($input, ['id_user' => $user->id]);

                $input['password'] = bcrypt($input['password']);

                User::create($input);

                /** response success */
                return response()->json([
                    'status'  => 200,
                    'message' => 'Success',
                    'data' => Arr::except($input, ['password', 'confirm_password'])
                ], 200);
            } catch (\Exception $error) {

                /** error create data user */
                return response()->json([
                    'status'  => 500,
                    'message' => $error->getMessage()
                ], 500);
            }
        } catch (\Exception $error) {

            /** error all progress */
            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }


    /**
     * Login
     */
    public function login(Request $request)
    {
        $credential = request(['username', 'password']);

        if (!Auth::attempt($credential)) {
            return response()->json([
                'status'  => 500,
                'message' => 'username and password doesn\'t match'
            ], 401);
        }

        $user        = $request->user();
        $tokenResult = $user->createToken($request->username);
        $token       = $tokenResult->token;

        $token->save();

        return response()->json([
            'status'  => 200,
            'message' => 'Success',
            'data'    => [
                'detail_user'  => users::where('id_user', $user->id_user)->first(),
                'access_token' => $tokenResult->accessToken,
                'expired_token_date' => OauthAccessTokens::where('id', $tokenResult->token->id)->select('expires_at')->get()[0]->expires_at
            ],
        ], 200);
    }


    /**
     * Function Logout
     */
    public function logout(Request $request)
    {

        $query = $request->user()->token()->delete();
        return response()->json([
            'status'  => 200,
            'message' => 'Logout Success',
        ], 200);
    }

    public function remove_old_token()
    {
        $response = [];
        $today = date("Y-m-d h:m:s");
        $data = OauthAccessTokens::whereDate('expires_at', '<', $today)->get();

        foreach ($data as $key => $value) {
            $request = OauthAccessTokens::where('created_at', $value->created_at)->delete();
            if ($request) {
                $response[] = $value->id;
            } else {
                return response()->json([
                    'status'  => 501,
                    'message' => 'Delete Old Token Failed at id = ' . $value->id
                ], 501);
            }
        }

        return response()->json([
            'status'  => 200,
            'message' => 'Delete Old Token Success'
        ], 200);
    }
}
