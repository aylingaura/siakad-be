<?php

namespace App\Http\Controllers;

use App\Helper\Helper;

# DB table used presensi, list_kelas
use App\Models\list_kelas;
use App\Models\presensi;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ListKelasController extends Controller
{
    public function list_kelas(Request $request)
    {
        try {
            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => [
                    'kelas' => Helper::queryParam($request->all(), 'App\Models\list_kelas')
                        ->select('id_kelas')
                        ->groupBy('id_kelas')
                        ->with('kelas:id_kelas,name,status')
                        ->get(),
                    'teacher' => Helper::queryParam($request->all(), 'App\Models\list_kelas')
                        ->select('id_teacher')
                        ->groupBy('id_teacher')
                        ->with('teacher:id_user,name')
                        ->get(),
                    'student' => Helper::queryParam($request->all(), 'App\Models\list_kelas')
                        ->select('id_list_kelas', 'id_student')
                        ->orderBy('updated_at', 'DESC')
                        ->with('student:id_user,name')
                        ->get()
                ]
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function list_student()
    {
        # DB table used m_user, list_kelas
        try {

            $data = DB::table('m_user')->leftJoin('list_kelas', function ($join) {
                $join->on('m_user.id_user', '=', 'list_kelas.id_student');
            })
                ->where('id_list_kelas', '=', null)
                ->where('id_user_type', '=', '3')
                ->select(
                    'm_user.id_user',
                    'm_user.name',
                )
                ->get();

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => $data
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function create_kelas(Request $request)
    {
        try {

            $input = $request->except(['created_at', 'created_by', 'updated_at', 'updated_by']);
            $list_kelas_post = [];

            foreach ($input['student'] as $key => $value) {
                $data = [
                    'id_kelas' => $input['id_kelas'],
                    'id_teacher' => $input['id_teacher'],
                    'id_semester' => $input['id_semester'],
                    'id_tahun_ajaran' => $input['id_tahun_ajaran'],
                    'id_student' => $value['id_student'],
                    'status' => $value['status'],
                ];

                $list_kelas_post[] = list_kelas::create(array_merge($data, ['created_by' => $request->user()->username]));
            }

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => $list_kelas_post
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function update_teacher(Request $request)
    {
        try {
            $input = $request->except(['created_at', 'created_by', 'updated_at', 'updated_by']);
            $data_post = [];
            $list_data = list_kelas::where('id_kelas', $request->id_kelas)
                ->where('id_teacher', $request->id_teacher_old)
                ->where('id_semester', $request->id_semester)
                ->where('id_tahun_ajaran', $request->id_tahun_ajaran)
                ->get();

            foreach ($list_data as $key => $value) {

                $data = [
                    'id_teacher' => $input['id_teacher'],
                    'status' => $value['status'],
                ];

                $data_post[] = list_kelas::where('id_list_kelas', $value->id_list_kelas)->update(array_merge($data, ['created_by' => $request->user()->username]));

                // restore if error
                if (!$data_post[0]) {
                    $data = [
                        'id_teacher' => $request->id_teacher_old,
                        'status' => $value['status'],
                    ];

                    list_kelas::where('id_list_kelas', $value->id_list_kelas)->update(array_merge($data, ['created_by' => $request->user()->username]));

                    return response()->json([
                        'status'  => 500,
                        'message' => 'Some data error'
                    ], 500);
                }
            }

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => $data_post
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function add_student(Request $request)
    {
        try {

            $input = $request->except(['created_at', 'created_by', 'updated_at', 'updated_by']);
            $list_kelas_post = null;

            $data = [
                'id_kelas' => $input['id_kelas'],
                'id_teacher' => $input['id_teacher'],
                'id_student' => $input['id_student'],
                'id_semester' => $input['id_semester'],
                'id_tahun_ajaran' => $input['id_tahun_ajaran'],
                'status' => true,
            ];

            $list_kelas_post = list_kelas::create(array_merge($data, ['created_by' => $request->user()->username]));

            return response()->json([
                'status'  => 200,
                'message' => 'success',
                'data'    => $list_kelas_post
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }

    public function delete_student($id_list_kelas)
    {
        try {
            $data = list_kelas::where('id_list_kelas', $id_list_kelas)->first();
            list_kelas::where('id_list_kelas', $id_list_kelas)->delete();

            return response()->json([
                'status'  => 200,
                'message' => 'delete success',
                'data'    => $data
            ], 200);
        } catch (\Exception $error) {

            return response()->json([
                'status'  => 500,
                'message' => $error->getMessage()
            ], 500);
        }
    }
}
