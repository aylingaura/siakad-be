-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 18, 2022 at 03:17 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_siakad`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_student`
--

CREATE TABLE `jadwal_student` (
  `id_jadwal_student` int(10) NOT NULL,
  `id_jadwal` int(10) DEFAULT NULL,
  `id_student` int(10) DEFAULT NULL,
  `created_at` varchar(25) DEFAULT NULL,
  `created_by` varchar(25) DEFAULT NULL,
  `updated_at` varchar(25) DEFAULT NULL,
  `updated_by` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jadwal_student`
--

INSERT INTO `jadwal_student` (`id_jadwal_student`, `id_jadwal`, `id_student`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 1, 3, NULL, NULL, NULL, NULL),
(2, 2, 4, NULL, NULL, NULL, NULL),
(3, 2, 3, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_teacher`
--

CREATE TABLE `jadwal_teacher` (
  `id_jadwal_teacher` int(10) NOT NULL,
  `id_jadwal` int(10) DEFAULT NULL,
  `id_teacher` int(10) DEFAULT NULL,
  `created_at` varchar(25) DEFAULT NULL,
  `created_by` varchar(25) DEFAULT NULL,
  `updated_at` varchar(25) DEFAULT NULL,
  `updated_by` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jadwal_teacher`
--

INSERT INTO `jadwal_teacher` (`id_jadwal_teacher`, `id_jadwal`, `id_teacher`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 1, 1, NULL, NULL, NULL, NULL),
(2, 2, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `list_kelas`
--

CREATE TABLE `list_kelas` (
  `id_list_kelas` int(4) NOT NULL,
  `id_kelas` int(4) DEFAULT NULL,
  `id_student` int(4) DEFAULT NULL,
  `id_teacher` int(4) DEFAULT NULL,
  `id_semester` int(4) DEFAULT NULL,
  `id_tahun_ajaran` int(4) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` varchar(25) DEFAULT NULL,
  `created_by` varchar(25) DEFAULT NULL,
  `updated_at` varchar(25) DEFAULT NULL,
  `updated_by` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `list_kelas`
--

INSERT INTO `list_kelas` (`id_list_kelas`, `id_kelas`, `id_student`, `id_teacher`, `id_semester`, `id_tahun_ajaran`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(5, 1, 3, 1, 1, 1, 1, '2022-03-13 22:55:16', 'admin', '2022-03-13 23:04:19', NULL),
(6, 1, 4, 1, 1, 1, 1, '2022-03-13 22:55:16', 'admin', '2022-03-13 23:04:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(249, '2014_10_12_000000_create_users_table', 1),
(250, '2014_10_12_100000_create_password_resets_table', 1),
(251, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(252, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(253, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(254, '2016_06_01_000004_create_oauth_clients_table', 1),
(255, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(256, '2019_08_19_000000_create_failed_jobs_table', 1),
(257, '2021_09_04_150216_cteate_m_kelas_table', 1),
(258, '2021_09_04_150216_cteate_m_mata_pelajaran_table', 1),
(259, '2021_09_04_150216_cteate_m_ruangan_table', 1),
(260, '2021_09_04_150216_cteate_m_semester_table', 1),
(261, '2021_09_04_150216_cteate_m_tahun_ajaran_table', 1),
(262, '2021_09_04_150216_cteate_m_user_type_table', 1),
(263, '2021_09_04_150216_cteate_pembagian_kelas_table', 1),
(264, '2021_09_04_150305_cteate_m_user_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_jadwal`
--

CREATE TABLE `m_jadwal` (
  `id_jadwal` int(5) NOT NULL,
  `id_kelas` int(5) DEFAULT NULL,
  `id_semester` int(5) DEFAULT NULL,
  `id_tahun_ajaran` int(5) DEFAULT NULL,
  `id_mata_pelajaran` int(5) DEFAULT NULL,
  `id_ruangan` int(5) DEFAULT NULL,
  `date` varchar(20) DEFAULT NULL,
  `start_time` varchar(15) DEFAULT NULL,
  `end_time` varchar(15) DEFAULT NULL,
  `is_open` tinyint(1) DEFAULT NULL,
  `created_at` varchar(25) DEFAULT NULL,
  `created_by` varchar(25) DEFAULT NULL,
  `updated_at` varchar(25) DEFAULT NULL,
  `updated_by` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_jadwal`
--

INSERT INTO `m_jadwal` (`id_jadwal`, `id_kelas`, `id_semester`, `id_tahun_ajaran`, `id_mata_pelajaran`, `id_ruangan`, `date`, `start_time`, `end_time`, `is_open`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 1, 1, 1, 1, 1, '2022-02-06', '10:00', '11:00', NULL, NULL, NULL, NULL, NULL),
(2, 1, 1, 1, 2, 1, '2022-02-06', '11:00', '12:00', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_kelas`
--

CREATE TABLE `m_kelas` (
  `id_kelas` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `m_kelas`
--

INSERT INTO `m_kelas` (`id_kelas`, `name`, `description`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'VII A', 'VII A', 1, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL),
(2, 'VII B', 'VII B', 1, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL),
(3, 'VIII A', 'VIII A', 1, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL),
(4, 'VIII B', 'VIII B', 1, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL),
(5, 'IX A', 'IX A', 1, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_mata_pelajaran`
--

CREATE TABLE `m_mata_pelajaran` (
  `id_mata_pelajaran` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `m_mata_pelajaran`
--

INSERT INTO `m_mata_pelajaran` (`id_mata_pelajaran`, `name`, `description`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'Matematika', 'Matematika', 1, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL),
(2, 'Bahasa Indonesia', 'Bahasa Indonesia', 1, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL),
(3, 'Bahasa Inggris', 'Bahasa Inggris', 1, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL),
(4, 'IPA', 'IPA', 1, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL),
(5, 'IPS', 'IPS', 1, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_ruangan`
--

CREATE TABLE `m_ruangan` (
  `id_ruangan` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `m_ruangan`
--

INSERT INTO `m_ruangan` (`id_ruangan`, `name`, `description`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, '101', '101', 1, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL),
(2, '102', '102', 1, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL),
(3, '103', '103', 1, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL),
(5, '201', '201', 1, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_semester`
--

CREATE TABLE `m_semester` (
  `id_semester` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `m_semester`
--

INSERT INTO `m_semester` (`id_semester`, `name`, `description`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'Ganjil/1', 'Ganjil/1', 1, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL),
(2, 'Genap/2', 'Genap/2', 1, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_tahun_ajaran`
--

CREATE TABLE `m_tahun_ajaran` (
  `id_tahun_ajaran` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `m_tahun_ajaran`
--

INSERT INTO `m_tahun_ajaran` (`id_tahun_ajaran`, `name`, `description`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, '2020', '2020', 1, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL),
(2, '2021', '2021', 1, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL),
(3, '2022', '2022', 1, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_user`
--

CREATE TABLE `m_user` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_user_type` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pob` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_province` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_city` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_district` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_village` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `education` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `m_user`
--

INSERT INTO `m_user` (`id_user`, `id_user_type`, `name`, `pob`, `dob`, `id_province`, `id_city`, `id_district`, `id_village`, `education`, `email`, `phone`, `photo`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, '2', 'Suwarno, S.Pd', 'Yogjakarta', '22-09-1990', '2', '2', '2', '3', 'S1', 'suwarno@gmail.com', '08912371238', NULL, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL),
(2, '2', 'Mario, S.Pd', 'Yogjakarta', '22-09-1990', '2', '2', '2', '3', 'S1', 'suwarno@gmail.com', '08912371238', NULL, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL),
(3, '3', 'Kazuya', 'Yogjakarta', '22-09-1990', '2', '2', '2', '3', 'S1', 'suwarno@gmail.com', '08912371238', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAHYgAAB2IBOHqZ2wAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAACAASURBVHic7d15mBXVnTfw76m6W/ftje4GuptmFZQl4oKjoiSi2Xw1b5ZJQDNONE6MS4xEQQUE9bKIiIpGHaO+STRmJk9C1hlnsjiZqEk0bnGNymLEZm1oeu+71HLq9/5xOwaR5fbtqjq3bv0+z9OPCPQ5P/pWfavq1KlTAqxsrF68eE5vOntB1jJnGKbdYppOrSntCtOScdt2dAJARAIQB/1+IUBCAJrQSNeFjOm6GYvr/RXRyN5YLPpuIhZ5Mx4Rv195592/8vdfxrxy8C2BlbQNG1KxV57tWdTdl/38QMaYlDGsatOSET8/zqiuycpEdCCZjG2rTsSer6yM/fvKdeuf8K0A5goOgABYv/6ain075SU9fcYFXX2ZmemsVVGaHx0hWRHNjqiq2DKipuJXx08+evX8K68cUF0VO7RS3IoY8jv97nfk+t3dvef39ht1wfyoCBXxqNFYW/mX+vrY2tXr7v2J6orY+wVxqypb8+bN0yc2N69r7+q/sKc/10ikuiJ36QJUV1OxZ2Rd9c+bJ2qLFi68K6u6prDjACgBa5ZeNXJnt7VhZ3vfGdKhUHwmgoCamlhPS0Ptd7fu3n39j3/8Y6m6pjAKxcZWqlZed80ZbR1939vblR4f5o9CCKLG2uSOllE1N6++466HVdcTJuHd6hRatnjBadt39D3W3W/Uq66l1CRiujWmqeaxcZPi/8yXCN7jAPDRmqVXjdy6N/fU7r0D0wT/5A9LABjdmHy7qb7qwlV33P0n1fWUq4jqAsJiwdeu+M9n39z3KSII3vmPjAC070tP3tuZfhqAprqecsUB4LFlixZ9cePW3Y9u3dHDP+sixKO6pbqGcsYbpUfmzZunN9aOePG1LbuOV11LEFUnopkxTXUPJ+vrF4BnD3iGT0Y9sGzhwlO3bO94KmvYMdW1uIEIIHLyv0D+9Hy/PwUBEBD5jUkI5C9xxJCvdQav+9smNDVctmzNbb9xoXR2BBwALrt+wYI7N7Z1LAzaJB6HHDhO/oscApEDInrvq1hCiMEvDZqW/6/QBAQ0aBpARKiuTBjjRtf+rHUKLl6w4F7DxX8WOwIOABddeellz2xr75utuo4jcRwHjpSQ5ICkhEMOVAXWjKOa99770EOj1fTOdNUFlIMNG1KxysTEXe2d6amqazkoItiOhGVbMA0DlmVBSpk/2is+VUlEo5ErL/7c7x57/MntSgsJKT4DGKb1qWvqX9jYuW0gYyVV17I/IoK0bdi2DemU/izbeDSC+rpKsyYZz1RXVuyrSsa2JRORTRWJxDON1fov5l+Z4qcKPcABMAy33HDD6Fc2b9uWM2XJDPZJmd/pbVviwOG6oIroGppH1Wab66vbGkdUP1mTiH730qWrXlBdVzngACjS+tQ19X/6S8fuUtj5iQi2ZcGyLeWn9H5pGVlnHDW2/uWR9TXfmXGq/siZZ6Zs1TUFEQdAEdavv6bi+Wc796VzVqXKOggE2wzXjn8wDXVJOe2o0S+MaRxx82XXpx5XXU+QcAAMUSqVimzZtLWzL23WqKqBiGCZJmxpKRu9L1WTxzb2TZ0w+jut0xNL5s9PmarrKXUcAEN00Zcu3tPVmxulqn/LMmGaFsrl+t4rTY011nHTWv+tDpO+flnqsozqekoVB8AQXHXZ5U+8u7t3roq+HSlhmAYcx1HRfWCNaqi2TpzW+r1/mFt5BY8TfBAHQIGWXL1g2RvvdKz2u18igmkYsCVvu8Mxrrk+M2va2CVXLVt9r+paSgkHQAFWL1582vMb254mn1frklLCMHKhHuBz2wnTxm49ZmLr2ZcuumGz6lpKAQfAEcybN08nimcNU0b96pMof61vWTyG5YXaqgpnzgmT7luUWvsN1bWoxgFwBFdccunLO/b2+/ZILxHByOUCMXsv6GYeM2bHhya1zrnk2uVtqmtRhVdaOYwbFn7jQj93fiklMtkM7/w+eW3TztbfvfDWlgfW3fRF1bWowg8DHcKGDanYU3/c8Zx0yJeQtKUN08jx3T2fDWQMfcu2fZ+//ILPNj3+1B//W3U9fuMAOITGmqNf6h4wmv3oy7YsGAY/Bq+KdBy8vX3fSRfN+z9nzf342Y8++eSToYlhvgQ4iBtvXDBr597+mX70ZVkmDJN3ftWICL9/8a8fSXd2bH44lUqorscvHAAH0fZO/2/8uONnmgZMk0f6S8mf39x21Gt72t/+t3tSyqZ6+4kD4ADLFi24vKsv1+B1P5ZtwbJ4wdtS9OrmnWNeeHP3pgdTDyp92MsPHAAH2NzW9U2v1+2Xtg2Tr/lL2utbdjVt2vvKxg0bUsof9/YSB8B+bli4YJnXz/dLRyJn5LzsgrnklU07xr70zL5XVNfhJQ6A/byzo2e5l+07jgMjx6+7C5LnX2+btnrZtWV7e5ADYNAN1119ZTpneTb6S0QwjCw/vx9ATzy36Zy7Vi69TXUdXuAAGLR1e9daL9s3DQOOw3t/EBEBv31u83UPrkt9QnUtbuMAQP5NPgMZq8qr9i3L4sd5Ay6bM8XTr7zzHz94MNWouhY3cQAA6Ojuf9irtslxYFk84l8OduzpTry+sfMp1XW4KfQBMG/ePL29a+AYL9omAnJGjq/7y8jzr7dN/+YtN9youg63hD4Axo0edT+RN49F27bJS3iVGSLCH1965+ZHv7l2nOpa3BD6AGjvHDjfi3aJiBf0KFOdPWm9p7//QtV1uCHUAbA+dU29V8t7G4bBp/5lasKYhkx1Mnqf6jrcEOoA2LHP8mSBSCklJI/6l6X62qQ8ddrYuRdfk+pRXYsbQh0Aezr7/68X7Zr8eG9ZSsQj9LFTps4vp/cShjYANmxIxfrSRrXb7Urb5oG/MjV31uTvX7449TPVdbgptAHw4jNdi71YE9Xkgb+ydOyUlvbrV91+keo63BbaAOjpzX7J7Tb56F+ekhUxOm7y+I+qrsMLEdUFqNLZm5nodpuWXdjRPxLR8OGTpmLm1PGYOG40GmqrEYtHEI+V9aPn7zFME6Zho7O3H1u37cFrG9vwhxc3wrZLMzxnz5zw2L9ce8ObquvwQijfC3D/rUtG/OpPbV1utikdiVz20I/6No6oxsVfmIuTTzgGI+pqIbTQnnwdFDkOurt78OzLm/DIT55EZ8+A6pIAAE2NteZnPzK5Yf6VqdIoyGWhPANoa89e63ab9iGW9zpt1hRcccHZaGkZjZDmbUGEpqG+oR7nfGw2zvnYqdi1cw/u//df408vbVFa1/FTWx4p150fCGkA9AzkPu1me0QE237/ff8Tp0/Akq/9IxoaPV9esAwJtIxpwurrL8K+fV1Yc99P8epG/1/eU1+blBNGVS7yvWMfhTIAegeyk9xsb/+dPxGPYs215+O4Y48BH/GHS6CxsQHrU1/Fy69twvI7f4ic4d9CqscfPebxcj76AyG9C5DNWRVutmfb+Y1y+uQx+Mn91+G4Y6eCd343CZwwcyp++sD1OG7qeH96FEBTS+0yXzpTKHRb6W2p61v/+NL27W615zgS2WwW5517Kr56wbk8uOcxchw88P3H8JNfPe9pP8dMGN31re98u+yv30K3tXb2ZL7iZnu2lLj0/I/i0i99ind+HwhNwxUXfhpX/NPHPe1nwpj6sl0IdH+h22LTOXuum+3948dn4bzPnAVAA0TofpxqCIEvfHouLj3/LM+6GFFb+YBnjZeQ0A0CGjnbtYUcGuqqcOmXPw/oCUD723tWCXBswDYAh6cFe+m8z56Fjs4+/Px/XnS13ca6pLx0UeoZVxstUaE7ZBmGVe9GOxFdw/pVVwHR5H47PwAIQIsCsSog6tk6owwAIHDlxZ/BlAlNrrY6rqV+m6sNlrDwBYAlk260s2rJJRhRf4Qs0WNAtOxfL6eU0DTcsfzLiETc25Trqirfcq2xEhfGABj2Zc+Zpx2LGdOnFPaX33d5wLxQVVWN5V/7vGvtJROR111rrMSFLgCcYS4AGtE1XPGV+UP7Jj00r5tXZs7smZg8YbQrbcUTkWddaSgAQhUAK6+99pThtnHxF89GIjHEHVoL3Vir74TQsMyts4B4nC8BypFB5gnD+f5YVMc5n/hwEd8ZuvlWSowbOwbTJ48ZdjsVJPe4UE4ghCoApETLcL7/s2efDl0v4mguOAB8IYCvX3j2sJrQNIFyWfCzEKEKAMuWRV8kEgGfOWeui9UwL0yZPB6xaPGXXLFIuC7XQhUAUlLRL3acOG4UqmtcX0OUuUzTdJx37qmqywiMkAWAHFHs9378jJPcLIV56IxTPqS6hMAIVwAQFT0174Tjp7lZCvNQ65hRqksIjFAFAERxo3G6JjCmyZ17zMx70VgcY5tcmfFd9sIVAEUuOjt65Ihis4MpcsIM1xd9LkvhCoAijW0peuyQKTLNhfkAYRCuABDFva+3eVTZLwxTdkY11KkuIRDCFQBFSiZdXUKQ+aAiEVVdQiBwABSgIhFXXQIbIv7MCsMBUABN4wHAoOHPrDAcAIyFGAcAYyHGAcBYiHEAMBZiHACMhRgHAGMhxgHAWIhxADAWYhwAjIUYBwBjIcYBwFiIcQAwFmIcAIyFGAcAYyHGAcBYiHEAMBZiHACMhRgHAGMhxgHAWIiFKgCEEEYx36dHdLdLYSUqEtGKWjo+qEIVAPGY/m4x31dbnXS5ElaqapMJU3UNfgpVADTUVv2hmO+bOJbfCxgWyWQ8rboGP4UqAKa0VP4AGNoZnpQ2aqorPaqIlZqmhto3Vdfgp1AFwPwrUwNjR1bvKfTvEwGmEaozwtCrr008rLoGP4UqAABg8oSRjxqGgSO9JZCIYBhZOFTkK4Xf1xi/pCIIRtVX211W7FHVdfgpdAHQJ2NLWkfXZHJGFlLKg/wNgm3byGYP9edF4P0/EE6c2vrdVCplq67DT6ELgFQq5Zxw9NgV5DjI5bLIpNPI5bIwcjnkchlkMmkYRg7kxpF/fxwCJa21qS5b29rzddV1+C10AQAAVy1ftW7O8ROfAQACQUoJW9qQ0jnipUHxQvmjDoSKRJQ+MmvieZdd9pCluha/hXarnHvuiDNmHj1ml28dCj4FKEURXcNZJx+94pKFKx9TXYsKoQ2AM89M2aed2TRxzqxJz/rSoQjtj7pkVVXG6bNnzVy06Oa1K1TXokpEdQEqzZ+fMgHMvv2mxd9+5rWtF/f2Zz3cSzkASslxx4zpnD5lzBe+es2NT6quRaVQB8DfXLfytkv+3913L9vV3vaj1zft/HBXnwdBwGcAymmawLRJTb2zprau+fI1N61TXU8p4AAY9NWrr94DYG4qldJGxeyL9vVmvtSfMcZlcmatpmn1GO4hXOMftZ+iEd2aMm6kEY9F0yNqKvaMrE++1FBXdc8Xr1j6suraSgmPTBWAerZ0Q6+oG14jEjB6XaqIHZGd2yyaTz5GdRmljs9LCyKGf3NQaOC89ZMLn1kIcAAUwnFjSqAANF5XwDcCWdUlBAEHQCEEilpI5AO0qCvNsAI4lFFdQhBwABSCXDqa8ECgj5xQPddfLA6AQhC5szGJCM8I9E+/6gKCgAOgINKdKcNCAIIvA/wh9qquIAg4AAohnFdca0vnAPCFEG+rLiEIOAAKYVhFrSV4UDwQ6A/N/pPqEoKAA6AQCeHexiQ0QIu51hw7GAIyxkuqqwgCHpEqEPW1ORC6Oz8vxwTMAVeaYgfh2IYYfWJCdRlBwGcAhbIt924raVF+OMhL5BS88GvY8VZYKLLfca8xAUT4AOUd2qS6gqDgACiUY/7a1fb0OPgKzCPS+R/VJQQFB0ChbPmtob5U5PAEEIm72B7LI8CgUC3tPRx8CBoC6n3HhBZz7z4eOYOPCPODa65xrB4xetYI1WUEBZ8BDIVtbHG1PaHxWIDbHPln1SUECQfAUNj2I663GUnwHQE3Cfqh6hKChC8BhoDoiQj6jzIhNHd/btIAXLzLGFokJUbKCiFOCt36/sXiQ88QCHGmDTuzzfWG9ThPEXaDtF7gnX9oOACGyjZWedJutJIfFR4usm9TXULQ8BZXBOp9V0KLuB+eMgdYvJBNUaSdFU0nVqouI2j4DKAYduZpT9rVE3wpUDT7P1RXEEQcAMWQNN+zt4hGk3xXYKjIIYiKq1WXEUS8pRVBjJrRDjuz1ZvGNSBaxeMBQyHNP4jRx/IDQEXgACiWnvsnzybwaREgwpezBSEHIHm56jKCig8zw0Bdm7chWjnWsw7sbP6LHZqdfVY0nzJbdRlBxWcAw5Hu/xTgeNd+pIIfGDosIkjzItVVBBkHwDCIsbNeg5nZ6GknkUpA5yXEDsrO/VK0fniz6jKCjANguGL6R0DSw8f5RP7OgM5nAu8jbRMR64uqywg6DoBhEtVTOmD03+1xLxwCB7KtpWLkHH75xzDxIKBLqPvtHkQStZ53ZBuAHfIHh6T5tmg6aYrqMsoBnwG4xTLOBHk4IPg3kfjgPAHvuypJjpSw7HNUl1EuOABcIkbNeBlG312+rO6jx4BoTfhmDBIAaS0UY091d2GWEAvrccQz1LXpr4gmJ/nUW34dAWn6051qVvZx0XLKJ1WXUU5CdgjxQW9sBqTp0zPpIn85EE2i7LNcmvvQnDlXdRnlhgPAZWLixBxy/Sd6e2vwAHociNeW74tHHWkC8mQhzrRVl1JuOAA8IJqO+wuMgX/2ZVDwvU41IFqdX1iknM4GyHFgZj8hmk7x5uGrkOMA8IgYOeMHyPWs9uyx4UPRE4MhUA6IYKa/LMae9pTqSsoVB4CHxKiZN8Lqvcv/EIiXwaQhAqzM1aL19O+rrqSccQB4TDR8aCGM7od8D4FIBQJ7KUAOYOcWi5bZ96gupdxxAPhAjJx5Gcy+b3n65OAHOtWC+RAROQSZu1I0n7JOdSlhwAHgE9E442swu6+GI/3rVIv415cbSEpY6c+J5lPvV11KWAT0HDG4qPOVTyJS90tPVhU+kLQAKyDPyzh2FiJ3uhh12suqSwkTDgAFqH3jRMQjryGSqPK0I8cEzAFPu3CFndsMSaeJ1lM6VZcSNhwAClHXW08gWjXXs4+h1N8zQA5gZ78tWmZ/VXUpYcVjAAqJ+mlnItP1NTi2N6ODTglPnCOZhhw4h3d+tfgMoARQ55YawH4Osaqp7n0kBBg98P3245GQA1jGf6Gl8wtCnGOoLifsOABKCO1+fT4qkt+DHksMuzErk78EKCWOuQdGbp4YN+cPqktheRwAJYi63rgHevLKou8UkA0YfS5XNQyOnYE0bhQts9erLoW9HwdAiSKiCDrf+Cmi1Z+CphceBCQBsx++Poh0KI6Vhm3+K1pOXSqEKIGC2IE4AEocEQl0vHUfYolLoMcOP7VPmvkFQvxYleiwdVg9sM3bMebUW4UQJTYIwfbHARAgtOnHEokGDVXNg4uAIH+kd+z8PX+Vo/5WBsh1AWa/I6aep6srhA1FwOaKhpy0NKTbgXR7fp5/vBpINKib828bQK47P97gvLcsGd9aDhAOgEAReO/0XppApjP/JfTB1YIrgURdfk0AL0gzv7NbacDOAY5PK58xz3AABMohLqdJ5k/BrQyQ2ZcPBC2SXyJMTwCRRP4sQYsCIgJoh7jycyh/B8Gx8zu4bQCOkd/xHTvfDysrPAYQELTll3FYPe7d2Bca/v7xk7t3DSgSFzPmh2Sp4mDj67WgiKPV1fbIyR/RSbp/y7Ai2exug8wrHABBYdpHqy6hYHaAag05DoCgkHK66hIKJuSHVJfACsMBEBSCPqK6hII5Aao15DgAgkKPJ/L3+0t83Da/IrFH9yGZ20p8a2IAQLRBR1tdOwiNAAHSAGxz/8k3ammR/I4fiWPwmNKBCT3NQszn+4YljucBBMHW2tMh0Jj/H5G/t68n8iP40spPyJEm/HsGQPx9XoEeO9hbikdiW92pAJ72qSBWJA6AINDEJQfdt4UORHQAg2fcJPebtDM4cWfYmSAATc9/iWh+p9cKmOrv4BJwAJQ8vgQocfT2b0ZBF214by8fagMO4Aze7wcNrhA0eN+fKP97QiC/KYj8r4WWD5e//bc4BkgbLyZ9bE+xDTDv8RlAqdO1JQAVP6gmNEDXAPj+5uA44FwPYJHfHbPC8RlACaO3fz0ZuvYGgAC+4gcAYMLRjhVHfWyz6kLYwfFtwBJF8+bp+NnLT8AJ7M4POIhlH/7d/9K8ebw+QIniAChVE6e+ge39rbjnCWBvAF7ucQCnow/dS3+Avhf/2jpQM/J11fWwg+MAKEGUWrEeWXkMAMARwKPPAy+2Ka6qcObvN6JrxU9h9mUBAOldndMyCxferrgsdhA8BlBiaOXqa7E3fXt+ZP4AlRHggpOA6tKcaEf9WaTv/y3S7+79wJ8JIVA1deyy5F23r1FQGjsEDoASQitXLsSezJ3QDnNi5jjACc3AWdNK59NzCOYvX0bfr1+FlId+tFjTBKqmjvtG5fp19/hYHTuMUtmEQo+W3vRD9FnnHXK1ngP/vjQgPjEdmNmKg54t+IEIeG070huew0BXYe8hEEIgObnlR1X3rj/f4+pYATgAFKP1qXrstl9Gzhk35O/t6gEqIhBzJgMnTwIqfLphkDWB57aCnt6M7J5u9FtDXxsw0TSiTUyoOLE2dVeXBxWyAnEAKESpFevQkbkWml7c50AEau8ALBuI6cCHWiFmtgLTxgBxl+d45Szgrd2g17cDf9kBmBI2Oeg2TVCR8431iE7JKS3rKu+6Y4m7xbJCcQAoQMtvWkO95kIBLT7sxnIGaG/n+38vogGTRgITGiHGNwLjG4CqIQ4cDuSAtk5Q2z5g6z5gawdwwEuMeywDpjP85cTiVQkzPq7x7sr1dy4edmNsSDgAfEJrVn0BvcZKpO1jIA43yldE2+0dgHmE0/B4BGioAuqrgGQMiOpAfHB6sGEBlgTSJtCVBjr7AePwLxmxyEG36e7LfSPxmBNvqNqoN1bfXLlu3U9cbZwdFAfAMNH6VD2yGActMg0mNYGcZkgaA4ua4MixZNitwqZE0af5hegfAHX7+zLQAdtERnr3uL+ua6RXxnOR6srtWlTfgWikXdO1nYhgN6LRdkeTb8mYvY3HEIaHA6BAtPab45DpTiEnT4Mpm2E7SUDoykbg92dYoD0dvnbZZeZgk/rX/mmaBj2mSz2RGNAqY+16ZfyPoqlyReXyW7arri0ISmDrLV20asWN1Gd+BVnZKkplZz8YxwHtaPe1yw4jV/Tgn9cEgEgiZut1yZ3xxqoHK+6441bVNZWqEt2i1aG1t3wSXdk7kLWn47AzckoLbdvla397jayv/Q1HJKpTdGTt1sTouuvit976M9X1lBIOgEG0YtU30J1ZDSmqVNcyZESg7bt97bLDyJbo8f/wEjUVudi4plTlHWtvU11LKQh9ANCqFTfSvtwy4Yjh35JTxbJBuz84/95LnWYOsgTGAIoVq6rIVUxoXBn2y4PQBgCtu+V47M78FjY1qK5l2NJZUGe3r1322SZyHt4F8EtiRFVf9JiJH02mlr+ouhYVQhcARCSw9Obfo9+cE6BL/MOijm4g6+81ec6R6LNKZFnyYdI0gcrxI5+q+ta9c1XX4rdQBQClUlPRbfwZjqhUXYtrpATt2ju4wKePCOi0gn0ZcKBYVUUudvTI2VVrbn9FdS1+KY9DYAFo5cqb0JF7s6x2fgDU0+//zg8AAqjUfV9o1FPmQDaRfXXHS+lrFq1SXYtfQnEGQEtv/l/0m2eV7H38YmWyoH3+XvsfqNcyYTjBHwvYn4BAcuKox6u+dc8nVdfitTLbIz6Ilix/FWlnpuo6XJczQB1dao7++yEAvaYBk4b/UFCpSbY2vlH97X8t6zcdl3UAONcu3ykMp0V1Ha7rT4O6e1VX8XcEDEgLGXn4B4iCKDGqdmfdow+1qq7DK2UbAHTd8rZiFtkoWQQgmwX1DRz5yT9FLHKQsW0YjgP/3lPovYqmEe/WPvLARNV1eKEsA4CW3Pgi0nKW6jqGxZGA5QCmCTKM/IIcAbnWdggwScJ2HFiOAwkHTsDzoGrcyJerHrrvRNV1uK3sAoCWpf4Lfea5qus4JFsClpWfvWfbgKTB9/c5+T3HGfx1GXIo//iQAwIRgQhwBEE6BEkObKKSvq1YPbn5F8n77v6c6jrcVFYBQCtX/gv2ZL9T6MKavjAtwDBAhgVkc8oH7UodgWA5DkyHYJMD05U3HLtDCIGqY8ddkVy37gHVtbilhPaU4aFb7h6N3Xt2QXzwZfW+s2xQOgMMZANz2l6qHACGlMhJG1YJ3GnQozolT5k4vlzWGyibtwNTx97NQuXO7xAwkAYNZAC7/EbDVdEAVOg6KnQdNjnIORJZKUGKzqSkJYX5RvtrAEYoKcBl6o+WLqCbb35YSNQo6dxxQL39oF17QD19vPN7KCI0VOlRNMYSSEai0BRN7Mp1D9QNLLj6USWduyzwlwCUun0UOjrbofl84U8E9KVBfYqm4jIQETLSRkbavg8T6LpOFcdNaK5as2aPz127KvBnAJTued73nT9ngNo7QL19vPMrJIRAMhJFfSyBmM9PdkophbOr6xlfO/VAoAOA1qz5lMg5433r0CFQZ3d+HX6LT/VLhS4E6qJxVEdiED5eFmTbuyflliz5hG8deiDYAbAv/bBvVzHm4Mq76eCshRc2FbqO+mgcEZ/GggmAsaMr0GMBgQ0AWrPudGE6jb50NpDJ7/x81C95uhAYEYujQvfnBleus3d05sals33pzAPBDYDO3h/58Xgv9fTlX8LJl/qBIQBUR6JIRrwPASLAbOva4HlHHglkANDatbUiJ8d43k9XD9A34HU3zCNJPYqaaMzzq0Szo7eVFq+t9bYXbwQyAJDOfcfr9fyooxsYyHjaB/NeQtNRE/H2tekOEdLp3YGcHhzIAKAew9OHfairx/dFNpl3EpqO6qi3y5dZe/o+7WkHHglcANAtqeMFYYjvuh5C+z19fOQvQxVaBMmIdyFgkUevfwAAA9FJREFUDmQqjaVLp3vWgUcCFwDIYoVng38DGb7mL2NJPeLZ3QEiwO7LrvCkcQ8FLwDS5umetGtZpbXMFvNEVSSKiEcHELsvfYYnDXsoeAFgyHrX2yTKr67L03rLngBQG417cmPA7M74My/FRYEKAFq9dg503fXPjrp6eZJPiOhCoMqDOwPStkVm6Y2nuN6whwIVALDMC1xv0zCBNA/6hU2FrnvyABFl0+5vox4KVgCY9kmutkeUv+XHQqkq4v4kIcoYJ7vboreCFQCWdPfJv/40n/qHWEQIVLp8V0BmzAmuNuixYAWA6bg33ZIov8Y+C7VKLeLqSYDMGoFaKixYAeCQe3Hdny7b5bdZ4TQhXJ0bQLaju9aYD4IWAO6ENRGoP+1KUyz4KiPunQWQdAK1TwWqWNeWe8lkAcnLdbM8DQIJl84CHHLpIOWTYAWAS4jn+rMDxDWXztwD9g60YAWAGycAtp2/98/YfmKaBt2F7YsCtnJMsALABcRr+rFDSOiBGr9zRegCANmc6gpYiYq5dRkQIOEKAMfJv6yTsYOICi1kO0TYAsAwVFfASlw0ZGcBoQoAyvHgHzu8qM9vGFItXP9aPv1nR+DXS0VKRdm8Hrwg/OBP6aipBkbWo6jH8bp7AI+e4vRqtaBSFZ4AkA7P/S8FTaMgLr8QOG768OZ1bH4H9OCjwF/b3KsN+WcDNCHghGR1qPCc79h89FeuYQTErTcAx88Y/qSuoydB3LIUGN/qTm37cWNCUFCEJwAkH/1VE+d9Bhjh4gt0EnGIL5/nXnuDwrNThOnfShwAys2a6X6bM6cDUXevZIVfb5wuAeEJgIA9pFGWEnH329Q1IObuAp8aXwKUoZAM6pS0d7e73+befa4v6spnAGWIOACUo5//yv02f/ZL19sM0f4fngBgJeDFV0GP/MidAVki4LHHgd88Ofy2Qiw88wBYafjFr0EvvAKcfjJEY5HrZ/b0g557Cfjru66WFkYcAMx/O9uBDf8ZsKUzylN4LgF4DICxDwhPAPAioKxAPBOwHBn8JCArTDRETwQG619a7Fm8YfKzAKxguhDFrwsQrFXBgxYARUznI4C6+zwohpWzqkgUxUwI0CNaoAabghUAyei7Q/0W6u4FTF4JiA1NVGioigz9JlmssWarB+V4JlgBMK56DqjA0TzHAe3rBgb4FWCsOJV6BDXRWME7STQRlVrL2NM8LcplwbpgAUB33jmWdnc/LbJ2KzTt/fVLB5A2KJMDBjK8AAhzhQMgK22YUkISwTlgMErXNYo11myPTG+enVyc2qWmyuL8f0o+mKJHzKIjAAAAAElFTkSuQmCC', '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL),
(4, '3', 'Kuroko', 'Yogjakarta', '22-09-1990', '2', '2', '2', '3', 'S1', 'suwarno@gmail.com', '08912371238', NULL, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL),
(5, '3', 'Fushi', 'Yogjakarta', '22-09-1990', '2', '2', '2', '3', 'S1', 'suwarno@gmail.com', '08912371238', NULL, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL),
(6, '3', 'Senku', 'Yogjakarta', '22-09-1990', '2', '2', '2', '3', 'S1', 'suwarno@gmail.com', '08912371238', NULL, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_user_type`
--

CREATE TABLE `m_user_type` (
  `id_user_type` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `m_user_type`
--

INSERT INTO `m_user_type` (`id_user_type`, `name`, `description`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'Admin', 'Admin', 1, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL),
(2, 'Teacher', 'Teacher', 1, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL),
(3, 'Student', 'Student', 1, '2021-11-13 08:41:33', 'Admin', '2021-11-13 08:41:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('05c5881eafe21ea7cddd0f88198e680e1e4399af8fb034d8d8a2da27bc9ebb24e1eda148e34dadcd', 1, 3, 'admin', '[]', 0, '2022-03-18 00:56:09', '2022-03-18 00:56:09', '2022-03-19 08:56:09'),
('0a6e319af3feeb6cd729c98c02bade05cd5501c77b07b86a87a5ab642e3f238c04091ee20e3eae5f', 1, 3, 'admin', '[]', 0, '2022-03-10 10:05:52', '2022-03-10 10:05:52', '2022-03-11 18:05:52'),
('0cd42ac847a811eed2cbec3becb4824737b7bb1cd7925768e66369ed7c8f7e7224441645c26e0053', 1, 3, 'admin', '[]', 0, '2022-03-15 06:38:41', '2022-03-15 06:38:41', '2022-03-16 14:38:41'),
('5699b1b08c78e7ee55af223ec3ca79f34630bd74c90fe452d14a00220f83741ac431e4e033d252d0', 1, 3, 'admin', '[]', 0, '2022-03-10 10:06:20', '2022-03-10 10:06:20', '2022-03-11 18:06:20'),
('586ffa41045a9cf1a90ddcf08648b31da05f1a6439183a8d3229373ccce2ddc01ca75894ddcd68d0', 1, 3, 'admin', '[]', 0, '2022-03-12 14:23:29', '2022-03-12 14:23:29', '2022-03-13 22:23:29'),
('5c32283bfb98c1b5230f8d425cddecfb20de7343b1078e9035976addd6bf5beb0876fe03f5f2893d', 1, 3, 'admin', '[]', 0, '2022-03-10 10:05:24', '2022-03-10 10:05:24', '2022-03-11 18:05:24'),
('665364540ebad420288465609996081204d8f6fd60841db30b432894898ff518ca1dc6df55b0e88a', 1, 3, 'admin', '[]', 0, '2022-03-14 22:48:35', '2022-03-14 22:48:35', '2022-03-16 06:48:35'),
('66d59e5e380bad038990f94be74ee3824559f8223b2bf16552ac2e6e6130af540fb8dd02b42931f8', 1, 3, 'admin', '[]', 0, '2022-03-10 10:01:39', '2022-03-10 10:01:39', '2022-03-11 18:01:39'),
('68ca1b884fefe8717ac59db93f503609d649da85ac694530321e4cda6396c6a5ad1244d955ce5545', 1, 3, 'admin', '[]', 0, '2022-03-10 10:03:16', '2022-03-10 10:03:16', '2022-03-11 18:03:16'),
('6fd3ad3794bc03dfec6515382ab9f0aa11ad531278814975182b6b0a4cfc50cd4e8c5a4caeddea47', 1, 3, 'admin', '[]', 0, '2022-03-10 10:04:53', '2022-03-10 10:04:53', '2022-03-11 18:04:53'),
('900a39e79e7be33dbacfb4f59e33628d18eddf5772389144c6d71e99745a774773cf76dcce09126b', 1, 3, 'admin', '[]', 0, '2022-03-10 10:03:30', '2022-03-10 10:03:30', '2022-03-11 18:03:30'),
('a2369279a9aa27534751f76e46a8f92c65a58fc6fae72fdfc4a7f94ba8e7e0f86f00fdd76eb9750f', 1, 3, 'admin', '[]', 0, '2022-03-13 14:09:53', '2022-03-13 14:09:53', '2022-03-14 22:09:53'),
('ad19e3b031d0799d1926b21d1a58f1dea1f3402eff45f77025ad195a11078fea84f27a9ea32f82fb', 1, 3, 'admin', '[]', 0, '2022-03-10 10:04:27', '2022-03-10 10:04:27', '2022-03-11 18:04:27'),
('fb6fc86b92a5abe9d36d4784408b758694dd5d1708e2de0ca5b71fcda324b24587cfcd0315247d5a', 1, 3, 'admin', '[]', 0, '2022-03-10 10:06:09', '2022-03-10 10:06:09', '2022-03-11 18:06:09');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'SN3H5ijGzxRnNJMRTUsf7u0TQTKpvPW96VjKbWZF', NULL, 'http://localhost', 1, 0, 0, '2021-11-13 01:45:58', '2021-11-13 01:45:58'),
(2, NULL, 'Laravel Password Grant Client', 'jH4MzjaB0a4xiZ2w21ptgedb1cWkM8GQeUTMKvGM', 'users', 'http://localhost', 0, 1, 0, '2021-11-13 01:45:58', '2021-11-13 01:45:58'),
(3, NULL, 'Laravel Personal Access Client', 'X6yNwAX3tOgqncFtcmBsc9dBJg1aYZasGySvd7Nt', NULL, 'http://localhost', 1, 0, 0, '2022-02-17 03:01:03', '2022-02-17 03:01:03'),
(4, NULL, 'Laravel Password Grant Client', 'oiEp9LwaGtk9K4x21Nd7pqD0ZhKdRtRjCfny5mHk', 'users', 'http://localhost', 0, 1, 0, '2022-02-17 03:01:03', '2022-02-17 03:01:03');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-11-13 01:45:58', '2021-11-13 01:45:58'),
(2, 3, '2022-02-17 03:01:03', '2022-02-17 03:01:03');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `presensi_student`
--

CREATE TABLE `presensi_student` (
  `id_presensi_student` int(10) NOT NULL,
  `id_jadwal_student` int(10) DEFAULT NULL,
  `is_present` tinyint(1) DEFAULT NULL,
  `created_at` varchar(25) DEFAULT NULL,
  `created_by` varchar(25) DEFAULT NULL,
  `updated_at` varchar(25) DEFAULT NULL,
  `updated_by` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `presensi_student`
--

INSERT INTO `presensi_student` (`id_presensi_student`, `id_jadwal_student`, `is_present`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 1, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `presensi_teacher`
--

CREATE TABLE `presensi_teacher` (
  `id_presensi_teacher` int(10) NOT NULL,
  `id_jadwal_teacher` int(10) DEFAULT NULL,
  `is_present` tinyint(1) DEFAULT NULL,
  `created_at` varchar(25) DEFAULT NULL,
  `created_by` varchar(25) DEFAULT NULL,
  `updated_at` varchar(25) DEFAULT NULL,
  `updated_by` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `id_user`, `id_user_type`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', '0', '1', NULL, '$2y$10$7n90yeujfsZWDjDYwqs/m.s2X74Hq0ldD0QppXLiMg06JZhb47hFm', NULL, '2021-11-13 01:41:32', '2021-11-13 01:41:32'),
(2, 'teacher', 'teacher', '1', '2', NULL, '$2y$10$bGPXseBjOqAsbw4arvSQJetVzg8sKpbtJ4Eq761eZ973EC2HRbLmq', NULL, '2021-11-13 01:41:32', '2021-11-13 01:41:32'),
(3, 'student', 'student', '4', '3', NULL, '$2y$10$ObotCbEE79t28c/Ki0JFXeEvTK72BRly0ioLyP4aBabs8Kk48BOSy', NULL, '2021-11-13 01:41:33', '2021-11-13 01:41:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `jadwal_student`
--
ALTER TABLE `jadwal_student`
  ADD PRIMARY KEY (`id_jadwal_student`);

--
-- Indexes for table `jadwal_teacher`
--
ALTER TABLE `jadwal_teacher`
  ADD PRIMARY KEY (`id_jadwal_teacher`);

--
-- Indexes for table `list_kelas`
--
ALTER TABLE `list_kelas`
  ADD PRIMARY KEY (`id_list_kelas`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_jadwal`
--
ALTER TABLE `m_jadwal`
  ADD PRIMARY KEY (`id_jadwal`);

--
-- Indexes for table `m_kelas`
--
ALTER TABLE `m_kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `m_mata_pelajaran`
--
ALTER TABLE `m_mata_pelajaran`
  ADD PRIMARY KEY (`id_mata_pelajaran`);

--
-- Indexes for table `m_ruangan`
--
ALTER TABLE `m_ruangan`
  ADD PRIMARY KEY (`id_ruangan`);

--
-- Indexes for table `m_semester`
--
ALTER TABLE `m_semester`
  ADD PRIMARY KEY (`id_semester`);

--
-- Indexes for table `m_tahun_ajaran`
--
ALTER TABLE `m_tahun_ajaran`
  ADD PRIMARY KEY (`id_tahun_ajaran`);

--
-- Indexes for table `m_user`
--
ALTER TABLE `m_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `m_user_type`
--
ALTER TABLE `m_user_type`
  ADD PRIMARY KEY (`id_user_type`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `presensi_student`
--
ALTER TABLE `presensi_student`
  ADD PRIMARY KEY (`id_presensi_student`);

--
-- Indexes for table `presensi_teacher`
--
ALTER TABLE `presensi_teacher`
  ADD PRIMARY KEY (`id_presensi_teacher`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jadwal_student`
--
ALTER TABLE `jadwal_student`
  MODIFY `id_jadwal_student` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jadwal_teacher`
--
ALTER TABLE `jadwal_teacher`
  MODIFY `id_jadwal_teacher` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `list_kelas`
--
ALTER TABLE `list_kelas`
  MODIFY `id_list_kelas` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=265;

--
-- AUTO_INCREMENT for table `m_jadwal`
--
ALTER TABLE `m_jadwal`
  MODIFY `id_jadwal` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `m_mata_pelajaran`
--
ALTER TABLE `m_mata_pelajaran`
  MODIFY `id_mata_pelajaran` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `m_ruangan`
--
ALTER TABLE `m_ruangan`
  MODIFY `id_ruangan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `m_semester`
--
ALTER TABLE `m_semester`
  MODIFY `id_semester` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `m_tahun_ajaran`
--
ALTER TABLE `m_tahun_ajaran`
  MODIFY `id_tahun_ajaran` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `m_user`
--
ALTER TABLE `m_user`
  MODIFY `id_user` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `m_user_type`
--
ALTER TABLE `m_user_type`
  MODIFY `id_user_type` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `presensi_student`
--
ALTER TABLE `presensi_student`
  MODIFY `id_presensi_student` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `presensi_teacher`
--
ALTER TABLE `presensi_teacher`
  MODIFY `id_presensi_teacher` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
