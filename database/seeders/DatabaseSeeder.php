<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use App\Models\user_type;
use App\Models\semester;
use App\Models\tahun_ajaran;
use App\Models\mata_pelajaran;
use App\Models\users;
use App\Models\kelas;
use App\Models\ruangan;

use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'name' => 'admin',
                'username' => 'admin',
                'id_user' => '0',
                'id_user_type' => '1',
                'password' => Hash::make('pass'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'teacher',
                'username' => 'teacher',
                'id_user' => '1',
                'id_user_type' => '2',
                'password' => Hash::make('pass'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'student',
                'username' => 'student',
                'id_user' => '4',
                'id_user_type' => '3',
                'password' => Hash::make('pass'),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]
        ];

        $user_type = [
            [
                'id_user_type' => '1',
                'name' => 'Admin',
                'description' => 'Admin',
                'created_by' => 'Admin',
                'status' => true,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'id_user_type' => '2',
                'name' => 'Teacher',
                'description' => 'Teacher',
                'created_by' => 'Admin',
                'status' => true,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'id_user_type' => '3',
                'name' => 'Student',
                'description' => 'Student',
                'status' => true,
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]
        ];

        $semester = [
            [
                'name' => 'Ganjil/1',
                'description' => 'Ganjil/1',
                'status' => true,
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Genap/2',
                'description' => 'Genap/2',
                'status' => true,
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]
        ];

        $tahun_ajaran = [
            [
                'name' => '2020',
                'description' => '2020',
                'status' => true,
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => '2021',
                'description' => '2021',
                'status' => true,
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => '2022',
                'description' => '2022',
                'status' => true,
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
        ];

        $mata_pelajaran = [
            [
                'name' => 'Matematika',
                'description' => 'Matematika',
                'status' => true,
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Bahasa Indonesia',
                'description' => 'Bahasa Indonesia',
                'status' => true,
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Bahasa Inggris',
                'description' => 'Bahasa Inggris',
                'status' => true,
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'IPA',
                'description' => 'IPA',
                'status' => true,
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'IPS',
                'description' => 'IPS',
                'status' => true,
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
        ];

        $student_teacher = [
            // teacher
            [
                'name' => 'Suwarno',
                'id_user_type' => '2',
                'pob' => 'Yogjakarta',
                'dob' => '22-09-1990',
                'id_province' => '2',
                'id_city' => '2',
                'id_district' => '2',
                'id_village' => '3',
                'education' => 'S1',
                'email' => 'suwarno@gmail.com',
                'phone' => '08912371238',
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Mario',
                'id_user_type' => '2',
                'pob' => 'Yogjakarta',
                'dob' => '22-09-1990',
                'id_province' => '2',
                'id_city' => '2',
                'id_district' => '2',
                'id_village' => '3',
                'education' => 'S1',
                'email' => 'suwarno@gmail.com',
                'phone' => '08912371238',
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            // student
            [
                'name' => 'Kazuya',
                'id_user_type' => '3',
                'pob' => 'Yogjakarta',
                'dob' => '22-09-1990',
                'id_province' => '2',
                'id_city' => '2',
                'id_district' => '2',
                'id_village' => '3',
                'education' => 'S1',
                'email' => 'suwarno@gmail.com',
                'phone' => '08912371238',
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Kuroko',
                'id_user_type' => '3',
                'pob' => 'Yogjakarta',
                'dob' => '22-09-1990',
                'id_province' => '2',
                'id_city' => '2',
                'id_district' => '2',
                'id_village' => '3',
                'education' => 'S1',
                'email' => 'suwarno@gmail.com',
                'phone' => '08912371238',
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Fushi',
                'id_user_type' => '3',
                'pob' => 'Yogjakarta',
                'dob' => '22-09-1990',
                'id_province' => '2',
                'id_city' => '2',
                'id_district' => '2',
                'id_village' => '3',
                'education' => 'S1',
                'email' => 'suwarno@gmail.com',
                'phone' => '08912371238',
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => 'Senku',
                'id_user_type' => '3',
                'pob' => 'Yogjakarta',
                'dob' => '22-09-1990',
                'id_province' => '2',
                'id_city' => '2',
                'id_district' => '2',
                'id_village' => '3',
                'education' => 'S1',
                'email' => 'suwarno@gmail.com',
                'phone' => '08912371238',
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
        ];

        $kelas = [
            [
                'id_kelas' => '1',
                'name' => 'VII A',
                'description' => 'VII A',
                'status' => true,
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'id_kelas' => '2',
                'name' => 'VII B',
                'description' => 'VII B',
                'status' => true,
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'id_kelas' => '3',
                'name' => 'VIII A',
                'description' => 'VIII A',
                'status' => true,
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'id_kelas' => '4',
                'name' => 'VIII B',
                'description' => 'VIII B',
                'status' => true,
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'id_kelas' => '5',
                'name' => 'IX A',
                'description' => 'IX A',
                'status' => true,
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
        ];

        $ruangan = [
            [
                'name' => '101',
                'description' => '101',
                'status' => true,
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => '102',
                'description' => '102',
                'status' => true,
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => '103',
                'description' => '103',
                'status' => true,
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => '104',
                'description' => '104',
                'status' => true,
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'name' => '201',
                'description' => '201',
                'status' => true,
                'created_by' => 'Admin',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
        ];

        // Send Data Seed
        User::insert($user);
        user_type::insert($user_type);
        semester::insert($semester);
        tahun_ajaran::insert($tahun_ajaran);
        mata_pelajaran::insert($mata_pelajaran);
        users::insert($student_teacher);
        kelas::insert($kelas);
        ruangan::insert($ruangan);
    }
}
