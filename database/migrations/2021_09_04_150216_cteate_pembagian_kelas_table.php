<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CteatePembagianKelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembagian_kelas', function (Blueprint $table) {
            $table->increments('id_pembagian_kelas', 2);
            $table->string('id_kelas', 10)->nullable();
            $table->string('id_teacher', 10)->nullable();
            $table->string('id_student', 10)->nullable();
            $table->tinyInteger('status')->nullable();
            $table->string('created_at', 25)->nullable()->useCurrent();
            $table->string('created_by', 25)->nullable();
            $table->string('updated_at', 25)->nullable()->useCurrent();
            $table->string('updated_by', 25)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembagian_kelas');
    }
}
