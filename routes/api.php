<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
use App\Http\Controllers\AuthController;

use App\Http\Controllers\Master\UserTypeController;
use App\Http\Controllers\Master\MataPelajaranController;
use App\Http\Controllers\Master\UserController;
use App\Http\Controllers\Master\TahunAjaranController;
use App\Http\Controllers\Master\SemesterController;
use App\Http\Controllers\Master\KelasController;
use App\Http\Controllers\Master\RuanganController;
use App\Http\Controllers\Master\JadwalController;

use App\Http\Controllers\ListKelasController;
use App\Http\Controllers\JadwalTeacherController;
use App\Http\Controllers\PresensiController;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('register', [AuthController::class, 'register']);

    Route::group(['middleware' => 'auth:api'], function () {

        Route::get('logout', [AuthController::class, 'logout']);
        Route::get('remove_old_token', [AuthController::class, 'remove_old_token']);

        # MASTER
        Route::get('master/user-type', [UserTypeController::class, 'index']);
        Route::get('master/user-type/{id_user_type}', [UserTypeController::class, 'detail']);
        Route::post('master/user-type', [UserTypeController::class, 'create']);
        Route::put('master/user-type/{id_user_type}', [UserTypeController::class, 'update']);
        Route::delete('master/user-type/{id_user_type}', [UserTypeController::class, 'delete']);

        Route::get('master/user', [UserController::class, 'index']);
        Route::get('master/user/{id_user}', [UserController::class, 'detail']);
        Route::post('master/user', [UserController::class, 'create']);
        Route::put('master/user/{id_user}', [UserController::class, 'update']);
        Route::delete('master/user/{id_user}', [UserController::class, 'delete']);

        Route::get('master/mata-pelajaran', [MataPelajaranController::class, 'index']);
        Route::get('master/mata-pelajaran/{id_mata_pelajaran}', [MataPelajaranController::class, 'detail']);
        Route::post('master/mata-pelajaran', [MataPelajaranController::class, 'create']);
        Route::put('master/mata-pelajaran/{id_mata_pelajaran}', [MataPelajaranController::class, 'update']);
        Route::delete('master/mata-pelajaran/{id_mata_pelajaran}', [MataPelajaranController::class, 'delete']);

        Route::get('master/tahun-ajaran', [TahunAjaranController::class, 'index']);
        Route::get('master/tahun-ajaran/{id_tahun_ajaran}', [TahunAjaranController::class, 'detail']);
        Route::post('master/tahun-ajaran', [TahunAjaranController::class, 'create']);
        Route::put('master/tahun-ajaran/{id_tahun_ajaran}', [TahunAjaranController::class, 'update']);
        Route::delete('master/tahun-ajaran/{id_tahun_ajaran}', [TahunAjaranController::class, 'delete']);

        Route::get('master/semester', [SemesterController::class, 'index']);
        Route::get('master/semester/{id_semester}', [SemesterController::class, 'detail']);
        Route::post('master/semester', [SemesterController::class, 'create']);
        Route::put('master/semester/{id_semester}', [SemesterController::class, 'update']);
        Route::delete('master/semester/{id_semester}', [SemesterController::class, 'delete']);

        Route::get('master/kelas', [KelasController::class, 'index']);
        Route::get('master/kelas/{id_kelas}', [KelasController::class, 'detail']);
        Route::post('master/kelas', [KelasController::class, 'create']);
        Route::put('master/kelas/{id_kelas}', [KelasController::class, 'update']);
        Route::delete('master/kelas/{id_kelas}', [KelasController::class, 'delete']);

        Route::get('master/ruangan', [RuanganController::class, 'index']);
        Route::get('master/ruangan/{id_ruangan}', [RuanganController::class, 'detail']);
        Route::post('master/ruangan', [RuanganController::class, 'create']);
        Route::put('master/ruangan/{id_ruangan}', [RuanganController::class, 'update']);
        Route::delete('master/ruangan/{id_ruangan}', [RuanganController::class, 'delete']);

        # KELAS
        Route::get('list-kelas', [ListKelasController::class, 'list_kelas']);
        Route::get('list-kelas/list_student', [ListKelasController::class, 'list_student']);
        Route::post('list-kelas/create-kelas', [ListKelasController::class, 'create_kelas']);
        Route::put('list-kelas/update-teacher', [ListKelasController::class, 'update_teacher']);
        Route::put('list-kelas/add-student', [ListKelasController::class, 'add_student']);
        Route::delete('list-kelas/delete-student/{id_list_kelas}', [ListKelasController::class, 'delete_student']);

        # JADWAL
        Route::get('jadwal/teacher/{id_teacher}/{date}', [JadwalTeacherController::class, 'list']);
        Route::get('jadwal/teacher/detail/{id_teacher}/{id_jadwal}', [JadwalTeacherController::class, 'detail']);
        
        Route::post('jadwal/create', [JadwalController::class, 'create']);
        Route::put('jadwal/update/{id_jadwal}', [JadwalController::class, 'update']);
        Route::delete('jadwal/delete/{id_jadwal}', [JadwalController::class, 'delete']);

        # PRESENSI
        Route::get('presensi/student', [PresensiController::class, 'student']);
        Route::get('presensi/kelas', [PresensiController::class, 'kelas']);
        Route::get('presensi/teacher', [PresensiController::class, 'teacher']);
    });
});
